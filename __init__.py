# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .location import ProductCategoryLocation, Location
from .product import Category
from .stock import ShipmentOut, ShipmentIn, ShipmentOutReturn


def register():
    Pool.register(
        Category,
        Location,
        ProductCategoryLocation,
        ShipmentOut,
        ShipmentIn,
        ShipmentOutReturn,
        module='stock_product_category_location', type_='model')
