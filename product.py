# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool

__all__ = ['Category']


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    locations = fields.One2Many(
        'stock.product.category.location', 'category', 'Default Locations')
    default_location = fields.Function(
        fields.Many2One('stock.location', 'Default location'),
        'get_default_location')

    def get_default_location(self, **kwargs):
        pool = Pool()
        CategoryLocation = pool.get('stock.product.category.location')

        _domain = self._get_default_location_domain(**kwargs)
        cat_locations = CategoryLocation.search(
            _domain, order=self._get_default_location_order())
        location = None

        if kwargs.get('with_parent', True):
            for cat_location in cat_locations:
                if cat_location.category.id == self.id:
                    location = cat_location.location
                    break

        if not location and cat_locations:
            location = cat_locations[0].location
        return location

    def _get_default_location_domain(self, **kwargs):
        _domain = [
            ('location.type', '=', kwargs.get('location_type', 'storage'))
        ]
        if kwargs.get('with_parent', True):
            _domain.append(('category', 'parent_of', self.id, 'parent'))
        else:
            _domain.append(('category', '=', self.id))
        if kwargs.get('warehouse', None):
            _domain.append(('warehouse', '=', kwargs['warehouse'].id))
        return _domain

    @staticmethod
    def _get_default_location_order():
        return [('sequence', 'ASC')]
